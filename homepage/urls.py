from django.urls import path
from . import views
from schedule.views import Join, sched_delete

app_name = 'RAD'

urlpatterns = [
    path('', views.index, name='index'),
    path('experience/', views.experience, name='experience'),
    path('education/', views.education, name='education'),
    path('skills/', views.skills, name='skills'),
    path('contact/', views.contact, name='contact'),
    path('portofolio/', views.portofolio, name='portofolio'),    
    path('schedule/', Join, name = 'Schedule'),
    path('<int:pk>',sched_delete, name = 'Delete'),
]