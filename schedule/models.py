from django.db import models

class Schedule(models.Model):
    Day = models.CharField(max_length=30)
    Date = models.DateField(blank = False)
    Time = models.TimeField(blank = False)
    Name = models.TextField(max_length=50)
    Location = models.TextField(max_length=30)
    Category = models.TextField(max_length=30)

def __str__(self):
	return self.Name
