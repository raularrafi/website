from django.shortcuts import render, redirect
from . import forms
from .models import Schedule as jadwal
from .forms import CreateSchedule

def Join(request):
    if request.method == "POST":
        form = CreateSchedule(request.POST)
        if form.is_valid():
            sched = jadwal()
            sched.Day = form.cleaned_data['Day']
            sched.Date = form.cleaned_data['Date']
            sched.Time = form.cleaned_data['Time']
            sched.Name = form.cleaned_data['Name']
            sched.Location = form.cleaned_data['Location']
            sched.Category = form.cleaned_data['Category']
            sched.save()
        return redirect('/schedule')
    else:
        sched = jadwal.objects.all()
        form = CreateSchedule()
        response = {"sched":sched, 'form' : form}
        return render(request,'schedule.html',response)

def sched_delete(request, pk):
    if request.method == "POST":
        form = CreateSchedule(request.POST or None)
        if form.is_valid():
            sched = jadwal()
            sched.Day = form.cleaned_data['Day']
            sched.Date = form.cleaned_data['Date']
            sched.Time = form.cleaned_data['Time']
            sched.Name = form.cleaned_data['Name']
            sched.Location = form.cleaned_data['Location']
            sched.Category = form.cleaned_data['Category']
            sched.save()
        return redirect('/schedule')
    else:
        jadwal.objects.filter(pk=pk).delete()
        data = jadwal.objects.all()
        form = CreateSchedule()
        response = {"sched":data, 'form' : form}
        return render(request, 'schedule.html', response)