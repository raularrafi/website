# Generated by Django 2.2.6 on 2019-10-08 06:01

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Day', models.CharField(max_length=30)),
                ('Date', models.DateField()),
                ('Time', models.TimeField()),
                ('Name', models.TextField(max_length=50)),
                ('Location', models.TextField(max_length=30)),
                ('Category', models.TextField(max_length=30)),
            ],
        ),
    ]
