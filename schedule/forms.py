from django import forms
from . import models

class CreateSchedule(forms.Form):
    Day = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Day',
        'type' : 'text',
        'required': True,
    }))

    Date = forms.DateField(widget=forms.DateInput(attrs={
        'class': 'form-control',
        'placeholder' : 'dd/mm/yyyy',
        'type' : 'date',
        'required': True,
    }))

    Time = forms.TimeField(widget=forms.TimeInput(attrs={
        'class': 'form-control',
        'type' : 'time',
        'required': True,
    }))

    Name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Activity Name',
        'required': True,
    }))

    Location = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Activity Location',
        'required': True,
    }))

    Category = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Activity Type',
        'required': True,
    }))

